import sha1 from 'sha1';
import AWS from 'aws-sdk';

AWS.config.region = "eu-west-1";
AWS.config.accessKeyId = "AKIAIPBKAMR2THQQPX3Q";
AWS.config.secretAccessKey = "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";

const dynamodb = new AWS.DynamoDB();

/**
 * @callback loginCallback
 * @param {Error} error
 * @param {boolean} authenticated
 * @param {string} username
 */

/**
 * checks login for unauthenticated
 *
 * @param username {string}
 * @param pass {string} bare password FIXME: unsafe to pass bare passwords
 * @param callback {loginCallback} callback when request is completed
 */
function login(username, pass, callback) {
    let password = sha1(pass);
    let params = {
        TableName: 'jvm-splash',
        ExpressionAttributeValues: {
            ":pass": {S: password},
            ":usr": {S: username}
        },
        KeyConditionExpression: "username = :usr AND password = :pass"
    };
    dynamodb.query(params, (err, data) => {
        if (err) {
            callback(err, false, undefined);
        } else {
            if ("Items" in data && data.Items.length > 0)
                callback(null, true, data.Items[0].username.S);
            else
                callback(null, false, undefined);
        }
    });
}

export default login;
